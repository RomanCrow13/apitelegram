﻿#Область ОбработчикиСобытийФормы

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	Если Не ПользователиСервер.ПроверитьНаличиеПользователя() Тогда
		Отказ = Истина;
		Возврат;
	КонецЕсли; 
	
	Настройки = ПользователиСервер.ПолучитьНастройкиПользователя();
	
	ПодключитьОбработчикОжидания("ОбновитьДанныеОбработки",?(Настройки.ИнтервалОбновленияДанных=0,1,Настройки.ИнтервалОбновленияДанных),Ложь);
	ОбновитьДанныеОбработки();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	ОбработкаВыбораНаСервере(ВыбранноеЗначение);
КонецПроцедуры

#КонецОбласти 

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ПроксиНажатие(Элемент, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	ФормаВыбораПрокси = ПолучитьФорму("Справочник.Прокси.ФормаВыбора",,ЭтотОбъект);
	ФормаВыбораПрокси.Параметры.РежимВыбора = Истина;
	ФормаВыбораПрокси.ЗакрыватьПриЗакрытииВладельца = Истина;
	ФормаВыбораПрокси.Открыть();
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыЧаты

&НаКлиенте
Процедура ЧатыВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	ОбновитьДеревоСообщений(Элемент.ТекущиеДанные.Значение);
КонецПроцедуры

#КонецОбласти 

#Область ОбработчикиСобытийЭлементовТаблицыФормыСообщения

&НаКлиенте
Процедура СообщенияВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ТекДанные = Элемент.ТекущиеДанные;
	ФормаПросмотра = ПолучитьФорму("Обработка.Чат.Форма.ФормаПросмотраСообщения",,ЭтотОбъект);
	
	Если ЗначениеЗаполнено(ТекДанные.ИДПолученного) Тогда
		ФормаПросмотра.Дата = ТекДанные.ДатаПолученного;
		ФормаПросмотра.Сообщение = ТекДанные.СообщениеПолученного;
		ФормаПросмотра.Отправитель = ТекДанные.ОтправительПолученного;
		ФормаПросмотра.Открыть();
	Иначе
		ФормаПросмотра.Дата = ТекДанные.ДатаОтправленногоСообщения;
		ФормаПросмотра.Сообщение = ТекДанные.ОтправленноеСообщение;
	КонецЕсли;
	
	ФормаПросмотра.Открыть();
	
КонецПроцедуры


#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Отправить(Команда)
	
	Если ОбменТелеграмСервер.ОтправитьСообщение(Элементы.Чаты.ТекущиеДанные.Значение,ТекстСообщения,Объект.Прокси) Тогда
		ТекстСообщения = "";
		ОбновитьДанныеОбработки();	
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти 

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ОбновитьДанныеОбработки()
	Если Объект.Прокси.Пустая() Тогда 
		УстановитьПрокси();
	КонецЕсли;
	
	ЗаполнитьЧатыЗначение();
	
	ТекЧат = Элементы.Чаты.ТекущиеДанные;
	Если Чаты.Количество() <> 0  Тогда
		Если ТекЧат = Неопределено Тогда
			ОбновитьДеревоСообщений(Чаты[0].Значение);
		Иначе
			ОбновитьДеревоСообщений(ТекЧат.Значение);
		КонецЕсли;	
	КонецЕсли;
		
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьЧатыЗначение()
	
	СписокНеобходимыхЧатов = ПолучитьСписокЧатов();
	
	НужноОбновить = Ложь;
	
	Для каждого Чат Из СписокНеобходимыхЧатов Цикл
		НайденыйЭлемент = ЭтотОбъект.Чаты.НайтиПоЗначению(Чат.Значение);
		Если НайденыйЭлемент = Неопределено Тогда
			НужноОбновить = Истина;	
		КонецЕсли; 
	КонецЦикла; 
	
	Если НужноОбновить Тогда
		ЭтотОбъект.Чаты = СписокНеобходимыхЧатов;	
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаВыбораНаСервере(ВыбранноеЗначение)
	
	Если ТипЗнч(ВыбранноеЗначение) = Тип("СправочникСсылка.Прокси") Тогда
		ОбработкаОбъект = РеквизитФормыВЗначение("Объект");
		ОбработкаОбъект.Прокси = ВыбранноеЗначение;
		ЗначениеВРеквизитФормы(ОбработкаОбъект,"Объект");	
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьПрокси()
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	Прокси.Ссылка КАК Ссылка
	|ИЗ
	|	Справочник.Прокси КАК Прокси
	|ГДЕ
	|	Прокси.Основной";
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		ОбработкаОбъект = РеквизитФормыВЗначение("Объект");
		ОбработкаОбъект.Прокси = Выборка.Ссылка;
		ЗначениеВРеквизитФормы(ОбработкаОбъект,"Объект");
	КонецЕсли; 
	 
КонецПроцедуры

&НаСервере
Процедура ОбновитьДеревоСообщений(Чат)
	
	СообщенияЧата = ПолучитьСообщенияЧата(Чат);
	
	ТаблицаСообщений = РеквизитФормыВЗначение("Сообщения");
	
	ТаблицаСообщений.Очистить();
	
	Для Каждого Сообщение Из СообщенияЧата Цикл
		НовСтрока = ТаблицаСообщений.Добавить();
		Если Сообщение.Полученное Тогда
			НовСтрока.ОтправительПолученного = Сообщение.Отправитель;
			НовСтрока.ДатаПолученного = Сообщение.Дата;
			НовСтрока.СообщениеПолученного = Сообщение.Сообщение;
			НовСтрока.ИДПолученного = Сообщение.ИДСообщения;
			Если Сообщение.Изменено Тогда
				НовСтрока.КомментарийПолученного = "Изменено " + Сообщение.ДатаИзменения;	
			КонецЕсли; 
		Иначе
			НовСтрока.ДатаОтправленногоСообщения = Сообщение.Дата;
			НовСтрока.ОтправленноеСообщение = Сообщение.Сообщение;
		КонецЕсли;
	КонецЦикла;	
	
	ЗначениеВРеквизитФормы(ТаблицаСообщений,"Сообщения");
	
КонецПроцедуры
 
&НаСервереБезКонтекста
Функция ПолучитьСообщенияЧата(Чат)
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Чат", Чат);
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ПолученныеСообщения.Отправитель КАК Отправитель,
	|	ПолученныеСообщения.Дата КАК Дата,
	|	ПолученныеСообщения.Сообщение КАК Сообщение,
	|	ПолученныеСообщения.Чат КАК Чат,
	|	ПолученныеСообщения.Изменено КАК Изменено,
	|	ПолученныеСообщения.ДатаИзменения КАК ДатаИзменения,
	|	ПолученныеСообщения.ИДСообщения КАК ИДСообщения
	|ПОМЕСТИТЬ ВТ_Полученные
	|ИЗ
	|	РегистрСведений.ПолученныеСообщения КАК ПолученныеСообщения
	|ГДЕ
	|	ПолученныеСообщения.Чат = &Чат
	|	И ПолученныеСообщения.Пользователь = &Пользователь
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ОтправленныеСообщения.Чат КАК Чат,
	|	ОтправленныеСообщения.Сообщение КАК Сообщение,
	|	ОтправленныеСообщения.Дата КАК Дата
	|ПОМЕСТИТЬ ВТ_Отправленные
	|ИЗ
	|	РегистрСведений.ОтправленныеСообщения КАК ОтправленныеСообщения
	|ГДЕ
	|	ОтправленныеСообщения.Чат = &Чат
	|	И ОтправленныеСообщения.Пользователь = &Пользователь
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТ_Полученные.Чат КАК Чат,
	|	ВТ_Полученные.Отправитель КАК Отправитель,
	|	ВТ_Полученные.Сообщение КАК Сообщение,
	|	ВТ_Полученные.Дата КАК Дата,
	|	ИСТИНА КАК Полученное,
	|	ВТ_Полученные.Изменено КАК Изменено,
	|	ВТ_Полученные.ДатаИзменения КАК ДатаИзменения,
	|	ВТ_Полученные.ИДСообщения КАК ИДСообщения
	|ИЗ
	|	ВТ_Полученные КАК ВТ_Полученные
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ВТ_Отправленные.Чат,
	|	NULL,
	|	ВТ_Отправленные.Сообщение,
	|	ВТ_Отправленные.Дата,
	|	ЛОЖЬ,
	|	ЛОЖЬ,
	|	ДАТАВРЕМЯ(1, 1, 1, 0, 0, 0),
	|	""""
	|ИЗ
	|	ВТ_Отправленные КАК ВТ_Отправленные
	|
	|УПОРЯДОЧИТЬ ПО
	|	Дата УБЫВ";
	Запрос.УстановитьПараметр("Пользователь",ПользователиСервер.ТекущийПользователь()); 
	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции

&НаСервереБезКонтекста
Функция ПолучитьСписокЧатов()
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ГруппыТелеграм.Ссылка КАК Ссылка,
	|	ГруппыТелеграм.Наименование КАК Представление,
	|	МАКСИМУМ(ЕСТЬNULL(Сообщения.Дата, ГруппыТелеграм.ДатаСоздания)) КАК Дата
	|ИЗ
	|	Справочник.ГруппыТелеграм КАК ГруппыТелеграм
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПолученныеСообщения КАК Сообщения
	|		ПО (Сообщения.Чат = ГруппыТелеграм.Ссылка)
	|ГДЕ
	|	ГруппыТелеграм.Пользователь = &Пользователь
	|
	|СГРУППИРОВАТЬ ПО
	|	ГруппыТелеграм.Ссылка,
	|	ГруппыТелеграм.Наименование
	|
	|УПОРЯДОЧИТЬ ПО
	|	Дата УБЫВ";
	Запрос.УстановитьПараметр("Пользователь",ПользователиСервер.ТекущийПользователь());	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Резульат = Новый СписокЗначений;
	Пока Выборка.Следующий() Цикл
		Резульат.Добавить(Выборка.Ссылка,Выборка.Представление);		
	КонецЦикла; 
	
	Возврат Резульат;
КонецФункции
  
#КонецОбласти 
