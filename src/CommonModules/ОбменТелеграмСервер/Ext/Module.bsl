﻿Функция ИнтернетПрокси(Прокси = Неопределено) Экспорт	
	
	Если Прокси = Неопределено Тогда
		ТекстУсловия = "Прокси.Основной";
	Иначе
		ТекстУсловия = "Прокси.Ссылка = &Прокси";
	КонецЕсли;
	
		ТекстЗапроса = 
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	Прокси.Сервер КАК Сервер,
		|	Прокси.Порт КАК Порт,
		|	Прокси.Пользователь КАК Пользователь,
		|	Прокси.Пароль КАК Пароль
		|ИЗ
		|	Справочник.Прокси КАК Прокси
		|ГДЕ
		|	%1";
		ТекстЗапроса = СтрШаблон(ТекстЗапроса,ТекстУсловия);
		Запрос = Новый Запрос;
		Запрос.УстановитьПараметр("Прокси",Прокси);
		Запрос.Текст = ТекстЗапроса;
		Выборка = Запрос.Выполнить().Выбрать();
		Если Выборка.Следующий() Тогда
			ПроксиСервер = Новый ИнтернетПрокси();
			ПроксиСервер.Порт("" + Выборка.Порт);
			ПроксиСервер.Сервер(Выборка.Сервер);
			ПроксиСервер.Пользователь = Выборка.Пользователь;
			ПроксиСервер.Пароль = Выборка.Пароль;
			ПроксиСервер.Установить("https","socks5://" + Выборка.Сервер, Выборка.Порт, Выборка.Пользователь, Выборка.Пароль, Ложь);
			Возврат ПроксиСервер;
		Иначе
			//запись в журнал регистрации?
		КонецЕсли;	
КонецФункции

Процедура ВыполнитьОбновлениеВФоне() Экспорт
	ФоновыеЗадания.Выполнить("ОбменТелеграмСервер.ОбновитьДанные");	
КонецПроцедуры

Процедура ОбновитьДанные() Экспорт
	
	Настройки = ПользователиСервер.ПолучитьНастройкиПользователя();
	ТекПользователь = Настройки.Пользователь;
	
	BotID = Настройки.ИДБота;
	Соединение = Новый HTTPСоединение("api.telegram.org", 443, , , ОбменТелеграмСервер.ИнтернетПрокси(), 20, Новый ЗащищенноеСоединениеOpenSSL(), Неопределено);
	
	Успешно = ПодключениеИПроверкаТелеграм(Соединение,BotID);
	Если Не Успешно Тогда
		Возврат;
	КонецЕсли;
	
	//получаем последние обновления
	ИДПоследнегоОбновления = ПолучитьИДПоследнегоОбновления(ТекПользователь);
	Если ИДПоследнегоОбновления = 0 Тогда
		ТекстОтбора = "";
	Иначе
		ТекстОтбора = "?offset=" + СтрЗаменить(Строка(ИДПоследнегоОбновления + 1),Символы.НПП,"");
	КонецЕсли; 
	Запрос = Новый HTTPЗапрос(BotID + "/getUpdates" + ТекстОтбора);
	HTTPОтвет = Соединение.Получить(Запрос);
	ЧтениеJSON = Новый ЧтениеJSON;
	ЧтениеJSON.УстановитьСтроку(HTTPОтвет.ПолучитьТелоКакСтроку("utf-8"));
	СтруктураОтвета = ПрочитатьJSON(ЧтениеJSON);
	
	Если СтруктураОтвета.Ok Тогда
		Для каждого Элемент Из СтруктураОтвета.result Цикл
			
			НовДействие = РегистрыСведений.СинхронизированныеДанные.СоздатьМенеджерЗаписи();
			НовДействие.Пользователь = ТекПользователь;
			НовДействие.ИДСообщения = Элемент.update_id;
			
			Если Элемент.Свойство("edited_message") Тогда
				
				НовДействие.Действие = Перечисления.ДействияВТелеграм.ИзменениеСообщения;
				
				ЭлементСообщение = Элемент.edited_message;
				ДатаСообщения = ДатаАпи(ЭлементСообщение.date,Настройки.ЧасовойПояс);
				Чат = ОбновитьДанныеОЧате(ЭлементСообщение.chat,ТекПользователь);
				Отправитель = ОбновитьДанныеОбУчастнике(ЭлементСообщение.from, Чат,ТекПользователь);
				
				НовСообщение = РегистрыСведений.ПолученныеСообщения.СоздатьМенеджерЗаписи();
				НовСообщение.Пользователь = ТекПользователь;
				НовСообщение.Чат = Чат;
				НовСообщение.Отправитель = Отправитель;
				НовСообщение.Дата = ДатаСообщения;
				НовСообщение.Изменено = Истина;
				НовСообщение.ДатаИзменения = ДатаАпи(ЭлементСообщение.edit_date,Настройки.ЧасовойПояс);
				НовСообщение.ИДСообщения = ЭлементСообщение.message_id;
				НовСообщение.Сообщение = ЭлементСообщение.text;
				НовСообщение.Записать();
				
			Иначе 
				ЭлементСообщение = Элемент.message;
				ДатаСообщения = ДатаАпи(ЭлементСообщение.date,Настройки.ЧасовойПояс);
				
				//ищем имеющийся чат
				Чат = ОбновитьДанныеОЧате(ЭлементСообщение.chat,ТекПользователь);
				
				Отправитель = ОбновитьДанныеОбУчастнике(ЭлементСообщение.from, Чат,ТекПользователь);
				
				Если ЭлементСообщение.Свойство("new_chat_member") Тогда
					НовДействие.Действие = Перечисления.ДействияВТелеграм.ДобавлениеУчастника;
					ОбновитьДанныеОбУчастнике(ЭлементСообщение.new_chat_member, Чат,ТекПользователь);
				КонецЕсли;
				
				Если ЭлементСообщение.Свойство("text") Тогда
					НовДействие.Действие = Перечисления.ДействияВТелеграм.Сообщение;
					
					НовСообщение = РегистрыСведений.ПолученныеСообщения.СоздатьМенеджерЗаписи();
					НовСообщение.Пользователь = ТекПользователь;
					НовСообщение.Чат = Чат;
					НовСообщение.Отправитель = Отправитель;
					НовСообщение.Дата = ДатаСообщения;
					НовСообщение.ИДСообщения = ЭлементСообщение.message_id;
					НовСообщение.Сообщение = ЭлементСообщение.text;
					НовСообщение.Записать();
				КонецЕсли;
				
				Если ЭлементСообщение.Свойство("left_chat_member") Тогда
					НовДействие.Действие = Перечисления.ДействияВТелеграм.УдалениеУчастника;
					
					НаборЗаписейУчастников = РегистрыСведений.УчастникиЧатов.СоздатьНаборЗаписей();
					НаборЗаписейУчастников.Отбор.Чат.Установить(Чат);
					НаборЗаписейУчастников.Отбор.Участник.Установить(ОбновитьДанныеОбУчастнике(ЭлементСообщение.left_chat_member, Чат,ТекПользователь));
					НаборЗаписейУчастников.Прочитать();
					НаборЗаписейУчастников.Очистить();
					НаборЗаписейУчастников.Записать();
				КонецЕсли;
				
			КонецЕсли;
			
			НовДействие.Дата = ДатаСообщения;
			НовДействие.Записать();
		КонецЦикла;
			
	КонецЕсли; 
	
КонецПроцедуры

Функция ОтправитьСообщение(Чат, Сообщение, Прокси = Неопределено) Экспорт
	Настройки = ПользователиСервер.ПолучитьНастройкиПользователя();
	BotID = Настройки.ИДБота;
	Соединение = Новый HTTPСоединение("api.telegram.org", 443, , , ИнтернетПрокси(Прокси), 20, Новый ЗащищенноеСоединениеOpenSSL(), Неопределено);
	
	Успешно = ПодключениеИПроверкаТелеграм(Соединение,BotID);
	Если Не Успешно Тогда
		Сообщить("Не удалось подключиться к серверу Telegram! Проверьте подключение к интернету или настройки прокси.");
		Возврат Ложь;
	КонецЕсли;
	
	Запрос = Новый HTTPЗапрос(BotID + "/sendMessage?chat_id=" + Чат.ИДГруппы + "&text=" + Сообщение);	
	HTTPОтвет = Соединение.Получить(Запрос);
	СтрокаОтвет = HTTPОтвет.ПолучитьТелоКакСтроку("utf-8");
	ЧтениеJSON = Новый ЧтениеJSON;
	ЧтениеJSON.УстановитьСтроку(СтрокаОтвет);
	СтруктураОтвета = ПрочитатьJSON(ЧтениеJSON);
	
	Если Не СтруктураОтвета.ok Тогда
		Если СтруктураОтвета.error_code = 403 Тогда
			Сообщить("Бот не может первым написать пользователю!");	
		КонецЕсли;
		Возврат Ложь;
	Иначе
		ЗаписьСообщения = РегистрыСведений.ОтправленныеСообщения.СоздатьМенеджерЗаписи();
		ЗаписьСообщения.Пользователь = Настройки.Пользователь;
		ЗаписьСообщения.Чат = Чат;
		ЗаписьСообщения.Сообщение = Сообщение;
		ЗаписьСообщения.Дата = ДатаАпи(СтруктураОтвета.result.date,Настройки.ЧасовойПояс);
		ЗаписьСообщения.Записать();
		
		Возврат Истина;
	КонецЕсли; 
	
КонецФункции

Функция ОбновитьДанныеОбУчастнике(Участник, Чат, Пользователь)
	
	ИДУчастника = СтрЗаменить(Строка(Участник.id),Символы.НПП,"");
	УчастникиПоИД = ОбщегоНазначения.ПолучитьПоРеквизиту("Справочник.УчастникиГруппы","ИДУчастника",ИДУчастника,Пользователь);
	
	Если УчастникиПоИД.Количество() = 0 Тогда
		//создаем участника в случае его отсутствия
		СпрУчастники = Справочники.УчастникиГруппы.СоздатьЭлемент();
		СпрУчастники.Пользователь = Пользователь;
		СпрУчастники.ЭтоБот = Участник.is_bot;
		Если Участник.is_bot Тогда
			СпрУчастники.Наименование = Участник.first_name;
		Иначе
			СпрУчастники.Наименование = Участник.first_name + " " + Участник.last_name;
		КонецЕсли; 
		
		Если Участник.Свойство("username") Тогда
			СпрУчастники.ИмяПользователя = Участник.username;	
		КонецЕсли; 
		
		СпрУчастники.ИДУчастника = ИДУчастника;
		СпрУчастники.Записать();
		УчастникСсылка = СпрУчастники.Ссылка;
	Иначе
		//проверка и актуализация данных
		УчастникСсылка = УчастникиПоИД[0];
	
	КонецЕсли;
	
	ЗаписьУчастника = РегистрыСведений.УчастникиЧатов.СоздатьМенеджерЗаписи();
	ЗаписьУчастника.Пользователь = Пользователь;
	ЗаписьУчастника.Чат = Чат;
	ЗаписьУчастника.Участник = УчастникСсылка;
	ЗаписьУчастника.Записать();
	
	Возврат УчастникСсылка;
КонецФункции

Функция ОбновитьДанныеОЧате(Чат,Пользователь)
	
	КодЧата = СтрЗаменить(Строка(Чат.id),Символы.НПП,"");	
	ЧатыПоИД = ОбщегоНазначения.ПолучитьПоРеквизиту("Справочник.ГруппыТелеграм","ИДГруппы",КодЧата,Пользователь);
	
	Если ЧатыПоИД.Количество() = 0 Тогда
		//создаем чат в случае его отсутствия
		СпрЧат = Справочники.ГруппыТелеграм.СоздатьЭлемент();
		СпрЧат.Пользователь = Пользователь;
		СпрЧат.ИДГруппы = КодЧата;
		СпрЧат.ТипЧата = Перечисления.ТипыЧатов[Чат.type];
		СпрЧат.ДатаСоздания = ТекущаяДата();
		Если СпрЧат.ТипЧата = Перечисления.ТипыЧатов.private Тогда
			СпрЧат.Наименование = Чат.first_name + " " + Чат.last_name;
		Иначе
			СпрЧат.Наименование = Чат.title;
		КонецЕсли; 
		СпрЧат.Записать();
		Возврат СпрЧат.Ссылка;
	Иначе
		//проверка и актуализация данных
		Возврат ЧатыПоИД[0];

	КонецЕсли;
	
КонецФункции

Функция ДатаАпи(Дата,ЧасовойПояс)
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ДОБАВИТЬКДАТЕ(&ПромежуточнаяДата, ЧАС, &ЧасовойПояс) КАК Дата";
	Запрос.УстановитьПараметр("ПромежуточнаяДата", '19700101' + Дата);
	Запрос.УстановитьПараметр("ЧасовойПояс", ЧасовойПояс);
	РезультатЗапроса = Запрос.Выполнить().Выбрать();
	РезультатЗапроса.Следующий();
	 
	Возврат РезультатЗапроса.Дата;		
КонецФункции
 
Функция ПодключениеИПроверкаТелеграм(Соединение,ИДБота) Экспорт
	Попытка		
		Запрос = Новый HTTPЗапрос(ИДБота + "/getMe");	
		HTTPОтвет = Соединение.Получить(Запрос);	
	Исключение
		СтрокаОписание = "Ошибка подключения:" + Символы.ПС + ОписаниеОшибки();
		//запись в журнал регистрации?            
		Возврат Ложь;;
	КонецПопытки;
	
	СтрокаОтвет = HTTPОтвет.ПолучитьТелоКакСтроку("utf-8");
	ЧтениеJSON = Новый ЧтениеJSON;
	ЧтениеJSON.УстановитьСтроку(СтрокаОтвет);
	СтруктураОтвета = ПрочитатьJSON(ЧтениеJSON);
	Если СтруктураОтвета.Ok Тогда
		Возврат Истина;
	Иначе
		СтрокаОписание = "Не найден бот:" + Символы.ПС + СтрокаОтвет;
		//запись в журнал регистрации?
		Возврат Ложь;
	КонецЕсли; 
КонецФункции

Функция ПолучитьИДПоследнегоОбновления(Пользователь) Экспорт
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	СинхронизированныеДанные.ИДСообщения КАК ИДСообщения
	|ИЗ
	|	РегистрСведений.СинхронизированныеДанные КАК СинхронизированныеДанные
	|ГДЕ
	|	СинхронизированныеДанные.Пользователь = &Пользователь
	|
	|УПОРЯДОЧИТЬ ПО
	|	ИДСообщения УБЫВ";
	Запрос.УстановитьПараметр("Пользователь",Пользователь);
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		Возврат Выборка.ИДСообщения;
	Иначе
		Возврат 0;
	КонецЕсли;   
КонецФункции
 