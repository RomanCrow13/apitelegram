﻿
Процедура ПриНачалеРаботыСистемы()
	
	Если Не ПользователиСервер.ПроверитьНаличиеПользователя() Тогда
		Форма = ПолучитьФорму("Обработка.НачалоРаботы.Форма");
		Форма.Открыть();
	Иначе
	
		ОбменТелеграмКлиент.ПодключитьОбработкуОбменаВФоне();

	КонецЕсли; 
	
КонецПроцедуры


 